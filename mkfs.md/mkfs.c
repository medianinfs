#include "../defs.h"
#include "../inode.h"

#include <fcntl.h>

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include <unistd.h>
#include <sys/stat.h>

struct PACKED superblock {
    uint16_t magic;
    uint32_t free_size;
    uint32_t max_size;
};

int main(int argc, char *argv[])
{
    char *fs = NULL;
    int fd = 0;
    struct superblock *sb= NULL;
    inode *rootdir = NULL;
#ifdef _HAS_TRACKED_INODE
    direntry *dentry = NULL;
#endif

    if (argc != 2){
        printf("Usage: %s [file]\n"
               "The filesystem needs a name",
               argv[0]);
        return -2;
    }

    fd = open(argv[1], O_WRONLY | O_CREAT, 0777);

    fs = calloc (BLOCKS*BLOCK_SIZE, sizeof(char));
    if (!fs){
        fprintf(stderr, "Cannot alloc %d bytes\n", (int) BLOCKS*BLOCK_SIZE);
        return (1);
    }
    sb = (struct superblock *) fs;

    sb->magic = 0xF514; 
    sb->max_size = BLOCK_SIZE * BLOCKPOOL_BLOCKS;
    sb->free_size = BLOCK_SIZE * BLOCKPOOL_BLOCKS;
    SET_BIT_1((char *)sb + FBB_OFFSET, 0);
    /* the first inode is the rootdir */
    SET_BIT_1((char*) sb + FIB_OFFSET, 1);


    rootdir = (inode *) sb + INODEPOOL_OFFSET + sizeof(inode);

    rootdir->type = I_DIR;
    rootdir->mode = S_IRUSR | S_IWUSR | S_IXUSR | S_IXGRP | S_IRGRP |
                    S_IROTH | S_IXOTH | S_IFDIR;
    rootdir->uid = getuid();
    rootdir->gid = getgid();
    rootdir->indirect=2;
    rootdir->atime = (uint32_t) time(NULL);
    rootdir->ctime = (uint32_t) time(NULL);
    rootdir->mtime = (uint32_t) time(NULL);

    rootdir->nlinks = 1;

#ifndef _HAS_TRACKED_INODE
    rootdir->size = 0;
#else
    rootdir->size = sizeof(direntry);
    /* First block */
    /*SET_BIT_1((char *)sb + FBB_OFFSET, 0); */
    dentry = (direntry *) sb + BLOCKPOOL_OFFSET;
    dentry->inode = 0;
    dentry->name[0] = '/';
    dentry->name[1] = '\0';
    dentry->symlink = 0;
#endif

    if ((write(fd, (void *) fs, BLOCKS*BLOCK_SIZE)) == -1){
        perror("write");
        close(fd);
        return -1;
    }
    close(fd);
    free(fs);

    return (0);
}
