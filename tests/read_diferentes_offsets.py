# -*- coding: utf-8 -*-

import sys
import random
random.seed("Yeah babe")

text = open(sys.argv[1]).read()

fin = open(sys.argv[1])
for _ in xrange(1000):
    offset = random.randint(0, len(text))
    size = random.randint(0, min(len(text) - offset - 1, 10 * 1024))
    fin.seek(offset)
    chunk = fin.read(size)
    assert len(chunk) <= size
    assert len(chunk) != 0
    if text[offset:offset + len(chunk)] != chunk:
        s = "Read fails for file {} with offset {} and size {}"
        raise IOError(s.format(sys.argv[1], offset, size))
print "Success!"
