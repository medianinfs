NEW_DISK="mkfs.md/disk.mdfs"

echo "Compilando..."
make > /dev/null
if [ $? -ne 0 ]; then
    echo "Error de compilacion"
    exit 1
fi

fusermount -u ./mnt 2> /dev/null && echo "Desmontando"

echo "Copiando $NEW_DISK"
cp ./$NEW_DISK .

if [ "$1" == "valgrind" ]; then
    valgrind --show-reachable=yes --leak-check=full ./fusewrapper mnt
else
    ./fusewrapper mnt
fi

exit 0
