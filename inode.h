#ifndef _INODE_H
#define _INODE_H

#define _GNU_SOURCE
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>

#include <time.h>

/* Define los tipos que pueden ser los inodos. */
enum {
    I_FILE = 0,
    I_DIR = 1,
};

/*
 * {4} size
 * {4} tipo (necesario para llegar a 32)
 * {2} mode
 * {2} uid
 * {2} gid
 * {12} atime,ctime, mtime
 * {2} indirect
 * {4} nlinks
 *
 * total 32 bytes
 */
struct _inode_t {
    uint32_t size; /* Tamaño del inodo */
    uint32_t type; /* tipo */

    uint16_t mode; /* acceso */
    uint16_t uid;
    uint16_t gid;

    uint32_t atime;
    uint32_t ctime;
    uint32_t mtime;

    uint16_t indirect;

    uint32_t nlinks;
} __attribute__((packed));

/* Sinonimo ? */
typedef struct _inode_t inode;

/* Asociacion de nombre y numero de inodo
 * 32 bytes tambien */
struct _direntry_t {
    uint32_t inum;
    char name[26];
    uint16_t symlink;
} __attribute__((packed));

typedef struct _direntry_t direntry;

/* Consigue un numero de inodo a partir de un path */
int16_t get_inode_from_path(const char *disk_mem, char *path, int16_t current);

/* Funciones del filesystem */
int inode_getattr(inode *i, ino_t inum, struct stat *stbuf);
int inode_chown(inode *i, uid_t uid, gid_t gid);
int inode_chmod (inode *i, mode_t mode);
int inode_utime(inode *i, const struct timespec tv[2]);
mode_t inode_get_mode (inode *i);

/* Consigue un nuevo inodo, devolviendo su numero y alocandolo */
uint16_t inode_allocate_block(const char *disk_mem);
/* Libera un bloque pasado por parametro */
void inode_free_block(const char *disk_mem, uint16_t pos);

void direntry_clean(direntry *dentry);
#endif
