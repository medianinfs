#ifndef _DEFS_H
#define _DEFS_H

/* Gatos */
/* Parámetros del disco */
#define BLOCKS 65536
#define BLOCK_SIZE 512
#define MDFS_FILENAME "disk.mdfs"
#define BLOCKPOOL_BLOCKS 64491
#define INODEPOOL_BLOCKS 1024
/* Parámetros de la organización lógica del disco */
/* superblock */
#define MDFS_SUPER_MAGIC (0x14F5)
#define SUPERBLOCK_SIZE (1*BLOCK_SIZE)
/* inode bitmap */
#define FIB_BLOCKS 4
#define FIB_SIZE (FIB_BLOCKS*BLOCK_SIZE)
/* free block bitmap */
#define FBB_SIZE (16*BLOCK_SIZE)
/* inode info */
#define INODE_ENTRIES 16384
#define INODE_SIZE 32
#define NAME_LENGTH 26
#define INODEPOOL_SIZE (1024*BLOCK_SIZE)
/* Posiciones absolutas de cada una de las secciones del disco */
#define SUPERBLOCK_OFFSET 0
#define FIB_OFFSET (SUPERBLOCK_OFFSET+SUPERBLOCK_SIZE)
#define FBB_OFFSET (FIB_OFFSET+FIB_SIZE)
#define INODEPOOL_OFFSET (FBB_OFFSET+FBB_SIZE)
#define BLOCKPOOL_OFFSET (INODEPOOL_OFFSET+INODEPOOL_SIZE)

/* Operaciones FBB */
#define SET_BIT_0(buf, i) ((buf)[(i)/8]&=~(1u<<(i)%8))
#define SET_BIT_1(buf, i) ((buf)[(i)/8]|=1<<(i)%8)
#define GET_BIT(buf, i) ((buf)[(i)/8]>>(i)%8&1)

/* Operaciones varias */
#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif

#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#define DIV_CEIL(a,b) (((a)+(b)-1)/(b))

/* No quiero huecos al medio */
#define PACKED __attribute__((packed))

/* Nos hartamos de poner lo mismo */
#define NEW_TIME (uint32_t) time(NULL)

#endif
