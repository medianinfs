#ifndef __MEDIANIN_H
#define __MEDIANIN_H

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <time.h>

#include "defs.h"
typedef struct medianin_s medianin;

/* Constructores y Destructores */
medianin *medianin_create(void);
void medianin_destroy(medianin *self);

/* Operaciones Básicas */
int medianin_getattr(medianin *self, const char *path, struct stat *stbuf);
int medianin_readlink(medianin *self, const char *path, char *buf, size_t size);
int medianin_mknod(medianin *self, const char *path, mode_t mode);
int medianin_unlink(medianin *self, const char *path);
int medianin_symlink(medianin *self, const char *from, const char *to);
int medianin_rename(medianin *self, const char *from, const char *to);
int medianin_link(medianin *self, const char *from, const char *to);
int medianin_chmod(medianin *self, const char *path, mode_t mode);
int medianin_chown(medianin *self, const char *path, uid_t uid, gid_t gid);
int medianin_truncate(medianin *self, const char *path, off_t size);
int medianin_read(medianin *self, const char *path,
                  char *buffer, size_t size, off_t offset);
int medianin_write(medianin *self, const char *path,
                   const char *buffer, size_t size, off_t offset);
int medianin_statfs(medianin *self, const char *path, struct statvfs *stbuf);
int medianin_utimens(medianin *self, const char *path,
                     const struct timespec tv[2]);

/* Operaciones sobre Directorios */
int medianin_mkdir(medianin *self, const char *path, mode_t mode);
int medianin_rmdir(medianin *self, const char *path);
int medianin_readdir(medianin *self, const char *path, void *buf,
                     fuse_fill_dir_t filler);

#endif /* __MEDIANIN_H */
