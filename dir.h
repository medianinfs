#ifndef _DIR_H
#define _DIR_H

#define _GNU_SOURCE
#include "inode.h"
#include <time.h>
#include <stdlib.h>
#include <fuse.h>


typedef inode dir;

/* Crea un directorio con el modo pasado por parametro.
 * Retorna el numero de inodo alocado. 0 en caso de error. */
uint16_t dir_create(const char *disk_mem, mode_t mode);

/* Agrega un direntry al directorio @d@ pasado por parametro.
 * En caso de error retorna 0. */
uint16_t dir_add_direntry(const char *disk_mem, dir *d, const char *name,
                          uint16_t inumber, uint16_t sym);
/* Quita un direntry del directorio y devuelve el numero de inodo a liberar
 * a posteriori */
uint16_t dir_remove_direntry(const char *disk_mem, dir *d, const char *name);

/* Busca un nombre por las direntry del dir. Retorna el inodo del nombre
 * buscado o 0 si no lo encuentra. */
uint16_t dir_search(const char *disk_mem, dir *d,const char *name);

/* Renombra un inodo pisando el nombre antiguo*/
int dir_rename_inode(const char *disk_mem, dir *d,
                     const char *old_name, const char *new_name);

/* Abstraccion del readdir */
uint16_t dir_readdir(const char *disk_mem, dir *d,
                     void *buf, fuse_fill_dir_t filler);

/* Retorna el ultimo directorio del path dado.
 * Ex: path=/usr/share/icons/archlinux.png, retorna el directorio de incons*/
dir *dir_get_from_path(const char *disk_mem, const char *path);

#endif
