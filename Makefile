TARGET=fusewrapper
CC=clang

# Agregado glib para el block walker
CFLAGS+= -O0 -ansi -Werror -Wall -Wdeclaration-after-statement \
		 -Wbad-function-cast -Wstrict-prototypes -Wmissing-declarations \
		 -Wmissing-prototypes -Wno-unused-parameter \
		 -Wunused-variable -g \
		 `pkg-config --cflags fuse glib-2.0`
LDFLAGS=`pkg-config --libs fuse glib-2.0`

SOURCES=$(shell echo *.c)
OBJECTS=$(SOURCES:.c=.o)

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) $^ -o $@ $(LDFLAGS)

clean:
	rm -f $(TARGET) $(OBJECTS) .depend *~

.depend: *.[ch]
	$(CC) -MM `pkg-config --cflags fuse glib-2.0` $(SOURCES) >.depend

-include .depend

.PHONY: clean all
