/* $Date: 2010-09-30 15:40:39 -0300 (Thu, 30 Sep 2010) $ */
/* $Revision: 1364 $ */

#include "log.h"
#include <stdarg.h>

static int loglevel=0;
static FILE *logfile=NULL;

int log_open(const char *filename, const int ll)
{
    loglevel = 0;
    if (filename == NULL) logfile = stdout;
    else logfile = fopen(filename, "w");
    if (logfile == NULL) return 1;
    loglevel = ll;
    return 0;
}

void log_close(void)
{
    fclose(logfile);
}

void debug1(const char * format, ...)
{
    va_list args;
    if (loglevel < 1 || logfile == NULL) return;
    va_start(args, format);
    vfprintf(logfile,format,args);
    va_end(args);
}

void debug2(const char *format, ...)
{
    va_list args;
    if (loglevel < 2 || logfile == NULL) return;
    va_start(args, format);
    vfprintf(logfile,format,args);
    va_end(args);
}

void debug3(const char *format, ...)
{
    va_list args;
    if (loglevel < 3 || logfile == NULL) return;
    va_start(args, format);
    vfprintf(logfile,format,args);
    va_end(args);
}
