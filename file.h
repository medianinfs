#ifndef _FILE_H
#define _FILE_H

#define _GNU_SOURCE

#include "inode.h"
#include "block_walker.h"

#include <sys/types.h>
#include <time.h>
#include <utime.h>
#include <sys/stat.h>
#include <sys/statvfs.h>

typedef inode file;

/* Crea un archivo vacio con el nombre y modo pasados por parametro.
 * Devuelve el numero de inodo asociado o 0 en caso de error. */
uint16_t file_create(const char *disk_mem, mode_t mode);

/* Abstraccion de lectura de un archivo */
size_t file_read (const char *disk_mem, file *f, char *buf,
                  size_t size, off_t offset);

/* Abstraccion de truncar un archivo */
int file_truncate (const char *disk_mem, file *f, off_t offset);

/* Abstraccion de escribir un archivo */
size_t file_write (const char *disk_mem, file *f, char *buf,
                   size_t size, off_t offset);

#endif
