#ifndef _BLOCK_WALKER_H
#define _BLOCK_WALKER_H
#include <stdint.h>

typedef struct _block_walker bwalker;

/* Creacion y destruccion */
bwalker *bwalker_create(const char *base_mem,size_t size, uint16_t *indirect_mem);
void bwalker_destroy (bwalker *bw);

/* Consigue un bloque libre. En caso de fallar, retorna 0 */
uint16_t bwalker_direct_length(bwalker *bw);
uint16_t bwalker_indirect_length(bwalker *bw);

uint16_t bwalker_allocate_block(bwalker *bw);
void bwalker_free_block(bwalker *bw);

uint16_t bwalker_next(bwalker *bw);
uint16_t bwalker_prev(bwalker *bw);
uint16_t bwalker_last_indirect (bwalker *bw);
uint16_t bwalker_last_block (bwalker *bw);
uint16_t bwalker_block_is_last(bwalker *bw);

void bwalker_set_first(bwalker *bw);
#endif
